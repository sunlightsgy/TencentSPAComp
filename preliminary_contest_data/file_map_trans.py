feature_map_file = open('./feature_map.txt')
feature_map_small = open('./feature_map_small.txt','w')

for line in feature_map_file.readlines():
    arr = line.strip().split('\t')
    #if not 'kw' in arr[0] and not 'topic' in arr[0]:
    features = arr[1].split(' ')
    features_num = [int(feature) for feature in features]
    feature_map_small.write(arr[0]+'\t')
    features_num.sort()
    for feature in features_num:
        feature_map_small.write(str(feature)+' ')
    feature_map_small.write('\n')



feature_map_file.close()
feature_map_small.close()