import operator

user_feature_file = open('./userFeature.data_pre')
feature_map_file = open('./feature_map_count_by_key.txt','w')
feature_map_value_file = open('./feature_map_count_by_value.txt','w')
feature_map = {}

num = 0
for line in user_feature_file.readlines():
    num += 1
    if num % 10000 == 0:
        print(num)

    features = line.strip().split('|')
    for i in range(1, len(features)):
        arr = features[i].split(' ')
        if arr[0] in feature_map:
            for feature in arr[1:]:
                if int(feature) not in feature_map[arr[0]]:
                    feature_map[arr[0]][int(feature)] = 1
                else:
                    feature_map[arr[0]][int(feature)] += 1
        else:
            feature_map[arr[0]] = {}
            for feature in arr[1:]:
                feature_map[arr[0]][int(feature)] = 1

user_feature_file.close()

for key in feature_map:
    feature_map_file.write(key+'\t')
    keys = list(feature_map[key].keys())
    keys.sort()
    for feature in keys:
        feature_map_file.write(str(feature)+':'+str(feature_map[key][feature])+' ')
    feature_map_file.write('\n')

    feature_map_value_file.write(key+'\t')

    keys = sorted(feature_map[key].items(),key=lambda x:x[1],reverse=True)
    for feature in keys:
        feature_map_value_file.write(str(feature[0])+':'+str(feature[1])+' ')
    feature_map_value_file.write('\n')

feature_map_file.close()
feature_map_value_file.close()