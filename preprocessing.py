import pandas as pd
import os
import gc
from scipy import sparse
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
from sklearn.feature_extraction.text import CountVectorizer

UF_FILE = './data_pre/userFeature.data'
UF_CSV = './data/user_features.csv'
DATA_CSV = './data_final.csv'
AF_FILE = './data_pre/adFeature.csv'
TR_FILE = './data_pre/train.csv'
TT_FILE = './data_pre/test2.csv'
SUB_FILE = './submission.csv'
user_one_hot_feature = ['age', 'carrier', 'consumptionAbility',
                   'education', 'gender', 'house']

ad_one_hot_feature = ['advertiserId', 'campaignId', 'creativeId',
                   'adCategoryId', 'productId', 'productType']


vector_feature = ['appIdAction', 'appIdInstall', 'interest1',
                  'interest2', 'interest3', 'interest4',
                  'interest5', 'kw1', 'kw2', 'kw3',
                  'topic1', 'topic2', 'topic3']

raw_feature = ['creativeSize',"aid_age_count",'aid_gender_count','campaignId_active_aid','appIdAction_score']



def get_user_feature():
    if os.path.exists(UF_CSV):
        print("Reading prepared user features csv file")
        user_feature = pd.read_csv(UF_CSV)
    else:
        with open(UF_FILE) as rf:
            userFeature_data = []
            for line in rf.readlines():
                userFeature_dict = {}
                for each in line.strip().split('|'):
                    each_list = each.split(' ')
                    userFeature_dict[each_list[0]] = ' '.join(each_list[1:])
                userFeature_data.append(userFeature_dict)
            # create dataframe from list of dictionaries
            user_feature = pd.DataFrame(userFeature_data)
            # save to csv file
            user_feature.to_csv(UF_CSV, index=False)
        gc.collect()
    return user_feature


def get_data():
    if os.path.exists(DATA_CSV):
        print("loading from prepared csv: ", DATA_CSV)
        return pd.read_csv(DATA_CSV)
    else:
        print("Preparing data")
        train = pd.read_csv(TR_FILE)
        predict = pd.read_csv(TT_FILE)
        # change labels from 0 to -1, for convenience
        train.loc[train['label'] == -1, 'label'] = 0
        predict['label'] = -1
        ad_feature = pd.read_csv(AF_FILE)
        user_feature = get_user_feature()
        data = pd.concat([train, predict])
        data = pd.merge(data, ad_feature, on='aid', how='left')
        data = pd.merge(data, user_feature, on='uid', how='left')
        data = data.fillna('-1')
        data.to_csv(DATA_CSV, index=False)
        print("data saved into: ", DATA_CSV)
        del user_feature
        return data


def get_test_data():
    predict = pd.read_csv(TT_FILE)
    predict['label'] = -1
    ad_feature = pd.read_csv(AF_FILE)
    user_feature = get_user_feature()
    predict = pd.merge(predict, ad_feature, on='aid', how='left')
    predict = pd.merge(predict, user_feature, on='uid', how='left')
    predict = predict.fillna('-1')
    predict.to_csv('data/data_test2.csv', index=False)
    del user_feature

#data = pd.read_csv('./data/data_final.csv')
#train = data[data['label'] != -1]
#predict = pd.read_csv('./data/data_test2.csv')
#data_new = pd.concat([train, predict])
#data_new.to_csv('data/data_final_new.csv', index=False)

outfile = open('preliminary_contest_data/statistic/hand_craft.txt','w')

data = pd.read_csv('./data/data_extend.csv')
for feature in user_one_hot_feature:
    outfile.write('aid_' + feature + '_count'+'\t')
    fl = data['aid_' + feature + '_count'].unique()
    for i in fl:
        outfile.write(str(i)+' ')
    outfile.write('\n')

for feature in ad_one_hot_feature:
    outfile.write(feature+"_active_aid"+'\t')
    fl = data[feature+"_active_aid"].unique()
    for i in fl:
        outfile.write(str(i)+' ')
    outfile.write('\n')

outfile.close()

#data = pd.read_csv('./data/data_test2.csv')
#print(data.shape)

# get_user_feature()
# get_data()
# get_test_data()
