import numpy
from sklearn import metrics

valid_file = open('./data_pre/ffm_valid.data')
y, y_pred = [], []
for line in valid_file.readlines():
  arrs = line.strip().split(' ')
  y.append(float(arrs[0]))
valid_file.close()
pred_file = open('./result/cffm_valid.out')
for line in pred_file.readlines():
  y_pred.append(float(line.strip()))
pred_file.close()
fpr, tpr, thresholds = metrics.roc_curve(y, y_pred, pos_label=1)
print(metrics.auc(fpr, tpr))
