import xgboost as xgb

MODEL_FILE = 'model/xgb_ext_1.model'
TRAIN_FILE = './data_pre/ext_feature_svm_train.data'
VALID_FILE = './data_pre/ext_feature_svm_valid.data'
TEST_FILE = './data_pre/ext_feature_svm_test.data'
RESULT_FILE = './result/xgb_ext_1.out'


def train():
    dtrain = xgb.DMatrix(TRAIN_FILE)
    dvalid = xgb.DMatrix(VALID_FILE)
    dtest = xgb.DMatrix(TEST_FILE)

    params = {'booster': 'gbtree',
              'objective': 'binary:logistic',
              'eval_metric': 'auc',
              'max_depth': 11,
              'lambda': 1,
              'subsample': 0.7,
              'colsample_bytree': 0.75,
              'min_child_weight': 5,
              'eta': 0.05,
              'scale_pos_weight': 1,
              'seed': 0,
              'silent': 1}
    print(params)

    watchlist = [(dtrain, 'train'), (dvalid, 'valid')]
    model = xgb.train(params, dtrain, num_boost_round=10000, evals=watchlist, early_stopping_rounds=150)
    # print(model.best_iteration)
    model.save_model(MODEL_FILE)
    limit = model.best_iteration
    y_pred = model.predict(dtest, ntree_limit=limit)
    #y_pred = model.predict(dtest)
    res_file = open(RESULT_FILE, 'w')
    for i in y_pred:
        res_file.write(str(i) + '\n')
    res_file.close()


def test():
    dtest = xgb.DMatrix(TEST_FILE)
    model = xgb.Booster(model_file=MODEL_FILE)  # init model
    y_pred = model.predict(dtest)
    res_file = open(RESULT_FILE, 'w')
    for i in y_pred:
        res_file.write(str(i) + '\n')
    res_file.close()


train()