import pandas as pd
import random

AF_FILE = './data_pre/adFeature.csv'

one_hot_feature = ['LBS', 'age', 'carrier', 'consumptionAbility',
                   'education', 'gender', 'house', 'os', 'ct',
                   'marriageStatus', 'advertiserId', 'campaignId', 'creativeId',
                   'adCategoryId', 'productId', 'productType']
vector_feature = ['appIdAction', 'appIdInstall', 'interest1',
                  'interest2', 'interest3', 'interest4',
                  'interest5', 'kw1', 'kw2', 'kw3',
                  'topic1', 'topic2', 'topic3']
user_one_hot_feature = ['age', 'carrier', 'consumptionAbility',
                   'education', 'gender', 'house']

ad_one_hot_feature = ['advertiserId', 'campaignId', 'creativeId',
                   'adCategoryId', 'productId', 'productType']

NUM_O_FEATURE = 10154

def get_feature_map():
    feature_map_file = open('preliminary_contest_data/statistic/feature_map_count_by_value.txt')

    feature_map = {}
    num = 0
    cat_num = 0

    for line in feature_map_file.readlines():
        arrs = line.strip().split('\t')
        feature_map[arrs[0]] = {}
        feature_map[arrs[0]]['cat'] = cat_num
        cat_num += 1

        for ele in arrs[1].split(' '):
            key = ele.split(':')[0]
            value = ele.split(':')[1]
            # print(key, value)
            if arrs[0] in one_hot_feature:
                feature_map[arrs[0]][key] = num
                num += 1
            elif arrs[0] in vector_feature:
                if int(value) > 5000:
                    feature_map[arrs[0]][key] = num
                    num += 1
                else:
                    feature_map[arrs[0]][key] = num
        num += 1

    feature_map_file.close()

    ad_feature = pd.read_csv(AF_FILE)
    for idx in ad_feature:
        feature_map[idx] = {}
        feature_map[idx]['cat'] = cat_num
        cat_num += 1
        for feature in ad_feature[idx].unique():
            feature_map[idx][str(feature)] = num
            num += 1
    hand_craft_file = open('preliminary_contest_data/statistic/hand_craft.txt')
    for line in hand_craft_file.readlines():
        arrs = line.split('\t')
        feature_map[arrs[0]] = {}
        feature_map[arrs[0]][cat_num] = {}
        cat_num += 1
        for i in arrs[1].split(' '):
            feature_map[arrs[0]][i] = num
            num += 1
    hand_craft_file.close()
    global NUM_O_FEATURE
    NUM_O_FEATURE = num
    return feature_map


def write_svm_data(svm_file, label, arrs, col_names):
    crt_num = NUM_O_FEATURE
    svm_file.write(label + ' ')
    t_map = {}
    for i in range(3, 39):
        if arrs[i] != "-1" and arrs[i] != '-1.0':
            features = arrs[i].split(' ')
#            print(col_names[i], features)
            for feature in features:
                feature = str(int(float(feature)))
                feature_trans = feature_map[col_names[i]][feature]
                if feature in t_map:
                    t_map[feature_trans] = t_map[feature_trans] + 1
                else:
                    t_map[feature_trans] = 1
    sort_keys = list(t_map.keys())
    sort_keys.sort()
    for feature_trans in sort_keys:
        svm_file.write(str(feature_trans) + ":" + str(t_map[feature_trans]) + " ")

    for i in range(39, len(arrs)):
        if arrs[i] != "-1.0":
            svm_file.write(str(crt_num) + ':' +str(arrs[i]) + ' ')
        crt_num += 1
    svm_file.write('\n')


def write_ffm_data(ffm_file, label, arrs, col_names):
    ffm_file.write(label + ' ')
    cat_map = {}
    t_map = {}
    for i in range(3, len(arrs)):
        if arrs[i] != "-1":
            features = arrs[i].split(' ')
            for feature in features:
                feature = str(int(float(feature)))
                feature_trans = feature_map[col_names[i]][feature]
                cat_map[feature_trans] = feature_map[col_names[i]]['cat']
                if feature in t_map:
                    t_map[feature_trans] = t_map[feature_trans] + 1
                else:
                    t_map[feature_trans] = 1
    sort_keys = list(t_map.keys())
    sort_keys.sort()
    for feature_trans in sort_keys:
        ffm_file.write(str(cat_map[feature_trans])+":"+str(feature_trans) + ":" + str(t_map[feature_trans]) + " ")
    ffm_file.write('\n')


# print(feature_map['aid'])

def cvt_data(data_func, method):
    data_file = open('./data/data_extend.csv')
    test_file = open('./data_pre/'+method+'_test.data', 'w')
    train_file = open('./data_pre/'+method+'_train.data', 'w')
    valid_file = open('./data_pre/'+method+'_valid.data', 'w')
    col_names = data_file.readline().strip().split(',')
    num = 0
    for line in data_file.readlines():
        if num % 10000 == 0:
            print(num)
        num += 1
        arrs = line.strip().split(',')
        label = arrs[2]
        if label == "-1":
            data_func(test_file, label, arrs, col_names)
        elif label == '0':
            if random.random() < 0.95:
                data_func(train_file, label, arrs, col_names)
            else:
                data_func(valid_file, label, arrs, col_names)
        elif label == '1':
            if random.random() < 0.95:
                data_func(train_file, label, arrs, col_names)
            else:
                data_func(valid_file, label, arrs, col_names)

    data_file.close()
    test_file.close()
    train_file.close()
    valid_file.close()

def cvt_test_data(data_func, method):
    data_file = open('./data/data_test2.csv')
    test_file = open('./data_pre/'+method+'_test2.data', 'w')
    col_names = data_file.readline().strip().split(',')
    num = 0
    for line in data_file.readlines():
        if num % 10000 == 0:
            print(num)
        num += 1
        arrs = line.strip().split(',')
        label = arrs[2]
        data_func(test_file, label, arrs, col_names)

    data_file.close()
    test_file.close()

feature_map = get_feature_map()
cvt_data(write_svm_data, 'ext_feature_svm')
