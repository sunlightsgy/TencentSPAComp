import xlearn as xl
# Training task
def train():
  ffm_model = xl.create_ffm() # Use field-aware factorization machine
  ffm_model.setTrain("./data_pre/ffm_train.data")  # Training data
  ffm_model.setValidate("./data_pre/ffm_valid.data")  # Validation data

  # param:
  #  0. binary classification
  #  1. learning rate: 0.2
  #  2. regular lambda: 0.002
  #  3. evaluation metric: accuracy
  param = {'task':'binary', 'lr':0.01,
         'lambda':0.00002,'k':8, 'metric':'auc', 'epoch':8000}

  # Start to train
  # The trained model will be stored in model.out
  ffm_model.fit(param, './ffm.model')

  # Prediction task
  ffm_model.setTest("./data_pre/ffm_test.data")  # Test data
  ffm_model.setSigmoid()  # Convert output to 0-1

  # Start to predict
  # The output result will be stored in output.txt
  ffm_model.predict("./ffm.model", "./result/output_ffm.txt")

def test():
  ffm_model = xl.create_ffm()
  ffm_model.setTest('./data_pre/ffm_valid.data')
  ffm_model.setSigmoid()
  ffm_model.predict('./ffm.model', './result/ffm_valid.out')

test()
