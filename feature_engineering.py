import numpy as np
import random
import pandas as pd
import scipy.special as special
import math
from math import log

user_one_hot_feature = ['age', 'carrier', 'consumptionAbility',
                   'education', 'gender', 'house']

ad_one_hot_feature = ['advertiserId', 'campaignId', 'creativeId',
                   'adCategoryId', 'productId', 'productType']

vector_feature = ['appIdAction', 'appIdInstall', 'interest1',
                  'interest2', 'interest3', 'interest4',
                  'interest5', 'kw1', 'kw2', 'kw3',
                  'topic1', 'topic2', 'topic3']

raw_feature = ['creativeSize',"aid_age_count",'aid_gender_count','campaignId_active_aid','appIdAction_score']


class HyperParam(object):
    def __init__(self, alpha, beta):
        self.alpha = alpha
        self.beta = beta

    def sample_from_beta(self, alpha, beta, num, imp_upperbound):
        # 产生样例数据
        sample = np.random.beta(alpha, beta, num)
        I = []
        C = []
        for click_ratio in sample:
            imp = random.random() * imp_upperbound
            # imp = imp_upperbound
            click = imp * click_ratio
            I.append(imp)
            C.append(click)
        return pd.Series(I), pd.Series(C)

    def update_from_data_by_FPI(self, tries, success, iter_num, epsilon):
        # 更新策略
        for i in range(iter_num):
            new_alpha, new_beta = self.__fixed_point_iteration(tries, success, self.alpha, self.beta)
            if abs(new_alpha - self.alpha) < epsilon and abs(new_beta - self.beta) < epsilon:
                break
            self.alpha = new_alpha
            self.beta = new_beta

    def __fixed_point_iteration(self, tries, success, alpha, beta):
        # 迭代函数
        sumfenzialpha = 0.0
        sumfenzibeta = 0.0
        sumfenmu = 0.0
        sumfenzialpha = (special.digamma(success + alpha) - special.digamma(alpha)).sum()
        sumfenzibeta = (special.digamma(tries - success + beta) - special.digamma(beta)).sum()
        sumfenmu = (special.digamma(tries + alpha + beta) - special.digamma(alpha + beta)).sum()

        return alpha * (sumfenzialpha / sumfenmu), beta * (sumfenzibeta / sumfenmu)


def nlp_feature_score(feature, train, predict):
    feature_count = {}
    feature_label_count = {}
    feature_list = train[feature].values.tolist()
    label_list = train['label'].tolist()
    for i in range(train.shape[0]):
        for item in feature_list[i].split(" "):
            if item in feature_count.keys():
                feature_count[item] += 1
            else:
                feature_count[item] = 1
            if (item not in feature_label_count.keys()) and (label_list[i] == 1):
                feature_label_count[item] = 1
            elif (item in feature_label_count.keys()) and (label_list[i] == 1):
                feature_label_count[item] += 1
    click = []
    convert = []
    for item in feature_label_count.keys():
        click.append(feature_count[item])
        convert.append(feature_label_count[item])
    c = pd.DataFrame({'click': click, 'convert': convert})
    hyper = HyperParam(1, 1)
    hyper.update_from_data_by_FPI(c['click'], c['convert'], 1000, 0.00000001)
    train_feature_score = []
    for i in range(train.shape[0]):
        score = 1
        for item in feature_list[i].split(" "):
            if item not in feature_label_count.keys():
                score += 0
            else:
                score = score * (1 - (feature_label_count[item] + hyper.alpha) / (
                        feature_count[item] + hyper.alpha + hyper.beta))
        train_feature_score.append(score)
    test_feature_score = []
    test_feature_list = predict[feature].values.tolist()
    for i in range(predict.shape[0]):
        score = 1
        for item in test_feature_list[i].split(" "):
            if item not in feature_label_count.keys():
                score += 0
            else:
                score = score * (1 - (feature_label_count[item] + hyper.alpha) / (
                        feature_count[item] + hyper.alpha + hyper.beta))
        test_feature_score.append(score)
    train = train.copy()
    train[feature + "_score"] = train_feature_score
    train.loc[train[feature] == "-1", feature + "_score"] = -1.0
    predict = predict.copy()
    predict[feature + "_score"] = test_feature_score
    predict.loc[predict[feature] == "-1", feature + "_score"] = -1.0
    return train, predict


data = pd.read_csv('./data/data_final_new.csv')

print("read data_final over")

for feature in user_one_hot_feature:
    print("processing "+feature)
    feature_count = data.groupby(['aid', feature]).size().reset_index().rename(columns={0: 'aid_'+feature+'_count'})
    data = pd.merge(data, feature_count, 'left', on=['aid', feature])

for feature in ad_one_hot_feature:
    print("processing "+feature)
    add = pd.DataFrame(data.groupby([feature]).aid.nunique()).reset_index()
    add.columns = [feature, feature+"_active_aid"]
    data = data.merge(add, on=[feature], how="left")

train = data[data['label'] != -1]
predict = data[data['label'] == -1]

for feature in vector_feature:
    print("processing "+feature)
    train, predict = nlp_feature_score(feature, train, predict)

data = pd.concat([train, predict])
print("shape", data.shape)

data.to_csv('data/data_extend.csv', index=False)