def print_dict(mydict,file_name):
    writer = open(file_name,'w')
    keys = list(mydict.keys())
    keys.sort()
    for key in keys:
        writer.write(str(key)+','+str(mydict[key])+'\n')
    writer.close()

train_file = open('./train.csv')

aid = {}
aid_true = {}

train_file.readline()
for line in train_file.readlines():
    arrs = line.strip().split(',')
    aid_n = int(arrs[0])
    if aid_n in aid:
        aid[aid_n] = aid[aid_n] + 1
    else:
        aid[aid_n] = 1
    if arrs[2] == "1":
        if aid_n in aid_true:
            aid_true[aid_n] = aid_true[aid_n] + 1
        else:
            aid_true[aid_n] = 1

print_dict(aid, './statistic/aid_all.csv')
print_dict(aid_true, './statistic/aid_true.csv')
train_file.close()