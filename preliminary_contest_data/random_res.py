import random

data_file = open('./test1.csv')
submission_file = open('./submission.csv','w')

submission_file.write('aid,uid,score\n')
data_file.readline() # delete headline
for line in data_file.readlines():
    submission_file.write(line.strip()+',%.8f\n' % random.random())

data_file.close()
submission_file.close()