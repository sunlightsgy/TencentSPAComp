# Transform ad features and concatenate them with user fetures 
# in train.csv to generate train.data_pre
# and test1.csv to generate test.data_pre
# Must Remember all the user features in memory, ~5g ram

# Transform ad features and store in dicts
adFeature_file = open('./data_pre/adFeature.csv')
ad_map = {}
line = adFeature_file.readline()
heads = line.strip().split(',')
for line in adFeature_file.readlines():
    arrs = line.strip().split(',')
    feature_str = ""
    for i in range(1,len(arrs)):
        feature_str += "|"+heads[i]+" "+arrs[i]
    print(feature_str)
    ad_map[arrs[0]] = feature_str
adFeature_file.close()

# Store user features in dict
userFeature_file = open('./data_pre/userFeature.data_pre')
user_map = {}

for line in userFeature_file.readlines():
    arrs = line.strip().split('|')
    uid = arrs[0].split(' ')[1]
    user_map[uid] = "|".join(arrs[1:])
userFeature_file.close()

# Generate train.data_pre
train_out = open('./data_pre/train.data_pre','w')
train_file = open('./data_pre/train.csv')
train_file.readline()
for line in train_file.readlines():
    arrs = line.strip().split(',')
    train_out.write(arrs[2]+' '+ad_map[arrs[0]]+'|'+user_map[arrs[1]]+'\n')
train_file.close()
train_out.close()

# Generate test.data_pre
test_out = open('./data_pre/test.data_pre','w')
test_file = open('./data_pre/test1.csv')
test_out.close()
test_file.readline()
for line in test_file.readlines():
    arrs = line.strip().split(',')
    test_out.write(ad_map[arrs[0]]+'|'+user_map[arrs[1]]+'\n')

test_file.close()
test_out.close()