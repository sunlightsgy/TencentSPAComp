import os

test_file = open('./data_pre/test2.csv')
res_files = {open('./result/xgb_test_1.out'): 0.5, open('./result/xgb_test_2.out'):0.5}
sub_file = open('./result/submission.csv','w')
sub_file.write('aid,uid,score\n')
test_file.readline()
for line in test_file.readlines():
  sub_file.write(line.strip()+',')
  score = 0.
  for res_file in res_files:
    score += float(res_file.readline().strip()) * res_files[res_file]
  sub_file.write('%.8f\n' % score)

for res_file in res_files:
  res_file.close()

test_file.close()
sub_file.close()
#os.system('zip ./cffm.zip ./result/submission.csv')
