from hyperopt import fmin, tpe, hp, space_eval, rand, Trials, partial, STATUS_OK
from sklearn import metrics
import xgboost as xgb

dtrain = xgb.DMatrix('./data_pre/svm_train_small.data')
dvalid = xgb.DMatrix('./data_pre/svm_valid.data')

valid_file = open('./data_pre/svm_valid.data')
y = []
for line in valid_file.readlines():
  arrs = line.strip().split(' ')
  y.append(float(arrs[0]))
valid_file.close()

def GBM(argsDict):
    max_depth = argsDict["max_depth"] + 5
    learning_rate = argsDict["learning_rate"] * 0.02 + 0.05
    subsample = argsDict["subsample"] * 0.1 + 0.7
    min_child_weight = argsDict["min_child_weight"]+1
    params={'booster':'gbtree',
    'objective': 'binary:logistic',
    'eval_metric': 'auc',
    'max_depth':max_depth,
    'subsample':subsample,
    'colsample_bytree':0.75,
    'min_child_weight':min_child_weight,
    'eta': learning_rate,
    'scale_pos_weight':1,
    'silent':1}
    print(params)

    watchlist = [(dtrain,'train'),(dvalid,'valid')]
    model=xgb.train(params,dtrain,num_boost_round=10000,evals=watchlist,early_stopping_rounds=200)
    y_pred = model.predict(dvalid)
    fpr, tpr, _ = metrics.roc_curve(y, y_pred, pos_label=1)
    metric = metrics.auc(fpr, tpr)
    print(metric)
    
    return -metric


space = {"max_depth": hp.randint("max_depth", 10),
         # [0,1,2,3,4,5] -> [50,]
         "n_estimators": hp.randint("n_estimators", 10),
         # [0,1,2,3,4,5] -> 0.05,0.06
         "learning_rate": hp.randint("learning_rate", 6),
         # [0,1,2,3] -> [0.7,0.8,0.9,1.0]
         "subsample": hp.randint("subsample", 4),
         "min_child_weight": hp.randint("min_child_weight", 5),
         }
algo = partial(tpe.suggest, n_startup_jobs=1)
best = fmin(GBM, space, algo=algo, max_evals=50)
print(best)
print(GBM(best))
